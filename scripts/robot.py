#!/usr/bin/env python

import rospy
import numpy as np
import math as m

#Message Stuff
from cse_190_assi_3.msg import AStarPath, PolicyList
from astar import a_star
from mdp import mdp
from q_learning import q_learn, q_policy
from std_msgs.msg import Bool

#Other files we have
from read_config import read_config
from copy import deepcopy

class Robot():
    def __init__(self):

      	#BY FIRE BE PURGED
        rospy.init_node("robot")

		
	#Publish to the particle cloud. New particle positions
        self.path_publisher = rospy.Publisher(
                "/results/path_list",
                AStarPath,
                queue_size = 10
        )

	#publish to the likelihood field after creating a new field
        self.policy_publisher = rospy.Publisher(
                "/results/policy_list",
                PolicyList,
                queue_size = 10,
                latch = True
        )

	#publish to when done
        self.finished_publisher = rospy.Publisher(
                "/map_node/sim_complete",
                Bool,
                queue_size = 10
        )
if __name__ == '__main__':
    rb = Robot()
    rospy.sleep(1)
    config = read_config()
    maxIterations =  config['max_iterations']
    threshDiff = config['threshold_difference']
    
    print "start astar"
    pathList = a_star();
    print(len(pathList));
    for i in pathList:
        rospy.sleep(0.3)
	rb.path_publisher.publish(i);
    print "done astar"
   
    map_size = config['map_size']
    goal = config['goal']
    walls = config['walls']
    pits = config['pits']

    
    valueMap = []
    #initialize the valueMap
    for row in range(map_size[0]):
      valueMap.append([])
      for col in range(map_size[1]):
	valueMap[row].append(0); 
    
    policyMap = []
    #initalize the policies, goal/wall/pits
    for row in range(map_size[0]):
      policyMap.append([])
      for col in range(map_size[1]):
	policyMap[row].append("");
    

    #set walls
    for e in walls:
      policyMap[e[0]][e[1]] = "WALL"

    #set pits  
    for p in pits:
      policyMap[p[0]][p[1]] = "PIT"
      valueMap[p[0]][p[1]] = config['reward_for_falling_in_pit']
    
    #set GOAL
    policyMap[goal[0]][goal[1]] = "GOAL"
    valueMap[goal[0]][goal[1]] = config['reward_for_reaching_goal']


    #print(valueMap)

    #initial sum
    sum1 = 0;
    sum2 = 0;
    
    #initialize tuple
    tupleMap = (valueMap, policyMap)

    #loop until the iterations
    for i in range(maxIterations):
      tupleMap = mdp(tupleMap[0], tupleMap[1]);

      value = np.array(tupleMap[0]);      
      value = value.flatten();


      policy = np.array(tupleMap[1]);      
      policy = policy.flatten();
      #print(value)

      sum1 = sum2;
      sum2 = sum(value);
      rb.policy_publisher.publish(policy);

      if(abs(sum1-sum2) <  threshDiff):
	#print(i)
	print("less than threshold");
	break;


    print(policy);
    #print "published True"
    
    #q_learning stuff
    valueMap = []
    #initialize the valueMap
    for row in range(map_size[0]):
      valueMap.append([])
      for col in range(map_size[1]):
	valueMap[row].append([]);
	for i in range(4):
	  if [row, col] in pits:
	    valueMap[row][col].append([config['reward_for_falling_in_pit'],1])
	  elif [row,col] in [goal]:
	    valueMap[row][col].append([config['reward_for_reaching_goal'],1])
	  else:
	    valueMap[row][col].append([0,1])
    
    print valueMap
    grid = valueMap
    for i in range(maxIterations):
	grid = q_learn(grid)

    for y in range(map_size[0]):
        for x in range(map_size[1]):
           print(x,y)
           print(grid[y][x])
    #print(grid)
    print(q_policy(grid))
    
    #rb.finished_publisher.publish(True);
    print("Shutdown....")
    #rospy.signal_shutdown("");
    rospy.spin()
